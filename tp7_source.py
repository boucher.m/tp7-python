"""TP7 une application complète
ATTENTION VOUS DEVEZ METTRE DES DOCSTRING A TOUTES VOS FONCTIONS
"""


def afficher_menu(titre,liste_options):
    """fonction qui à partir d’un titre et d’une liste d’options données sous la forme d’une
    liste de str affiche le menu

    Args:
        titre (str): le titre
        liste_options (list): la liste des options
    """    
    cpt = 1
    print("+-------------------------+\n|",titre.ljust(23),"|\n+-------------------------+")
    for i in range(len(liste_options)):
        print(cpt,"->" ,liste_options[i])
        cpt += 1
    rep = input ("Entrez votre choix " "[1-"+str(len(liste_options))+"]:" )
    return rep

#afficher_menu("MENU TITRE",("salut","salu","sal","sa","s"))







def demander_nombre(message,borne_max):
    """prend en paramètre un message d’invite et un entier positif borne_max
    et qui affiche à l’utilisateur le message et attend sa réponse. La fonction doit retourner None si
    l’entrée de l’utilisateur est incorrecte. Sinon elle retourne le nombre entier compris entre 1 et
    borne_max

    Args:
        message (str): le message d'invite
        borne_max (int): le chiffre maximum autorisé

    Returns:
        int ou None: le nbr si il est bon sinon None
    """    
    nbr =  input(message)
    if not nbr.isdecimal():
        return None
    elif int(nbr) > borne_max:
        return None
    else: 
        return nbr

#demander_nombre("prendre un nombre entre 1 et 8 :",8)





def menu(titre,liste_options):
    """fonction qui à partir d’un titre et d’une liste d’options données sous la forme d’une
    liste de str affiche le menu et demande à l’utilisateur sa réponse. La fonction retourne le numéro
    de l’option du menu choisie par l’utilisateur ou None si celui-ci a entré une valeur incorrecte

    Args:
        titre (str): le titre
        liste_options (list): une liste de str (les options du menu)

    Returns:
        int or None: le nombre correspondant à l'option ous si il y a une erreur None
    """    
    rep = afficher_menu(titre, liste_options)
    if rep.isdecimal() and int(rep) <= len(liste_options):
        return int(rep)
    else: 
        return None
#menu("MENU TITRE",("salut","salu","sal","sa","s"))


def programme_principal():
    liste_options=["Charger un fichier","Rechercher la population d'une commune","Afficher la population d'un département","Quitter"]
    liste_communes=[]
    while True:
        rep=menu("MENU DE MON APPLICATION",liste_options)
        if rep is None:
            print("Cette option n'existe pas")
        elif rep==1:
            print("Vous avez choisi",liste_options[rep-1])
            liste_communes = input("quel est le nom de votre fichier ? :" )
            print(charger_fichier_population(liste_communes))
        elif rep==2:
            print("Vous avez choisi",liste_options[rep-1])
            commune = input("de quelle commune voulait vous connaitre la population? :" )
            print("la population de "+commune+"est de "+population_d_une_commune(liste_communes,commune))
        elif rep==3:
            print("Vous avez choisi",liste_options[rep-1])
        else:
            break
        input("Appuyer sur Entrée pour continuer")
    print("Merci au revoir!")
   
###### exercice 1 terminé ######


def charger_fichier_population(nom_fic):
    """fonction qui prend en paramètres un nom de fichier et qui produit la liste de tuples
    (DEPCOM,COM,PTOT) contenue dans le fichier

    Args:
        nom_fic (str): le nom du fichier

    Returns:
        list: une liste de tuples de (DEPCOM,COM,PTOT)
    """    
    fic = open(nom_fic,'r')
    fic.readline()
    liste = []
    for ligne in fic:
        elt = ligne.split(";")
        liste.append((int(elt[0]), elt[1], int(elt[4])))
    fic.close()
    return liste 
#print(charger_fichier_population("extrait1.csv"))

def population_d_une_commune(liste_pop,nom_commune):
    res =  0 
    for ind in range(len(liste_pop)):
        if nom_commune == liste_pop[ind][1]:
            res = liste_pop[ind][2]
    return res 

def liste_des_communes_commencant_par(liste_pop,debut_nom):
    ...

def commune_plus_peuplee_departement(liste_pop, num_dpt):
    ...

def nombre_de_communes_tranche_pop(liste_pop,pop_min,pop_max):
    ...

def place_top(commune, liste_pop):
    ...

def ajouter_trier(commune,liste_pop,taille_max):
    ...
    

def top_n_population(liste_pop,nb):
    ...

def population_par_departement(liste_pop):
    ...

def sauve_population_dpt(nom_fic,liste_pop_dep):
    ...









#appel au programme principal
programme_principal()
